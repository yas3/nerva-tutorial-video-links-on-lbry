
# Windows

## Installing Nerva

_[How to install Nerva GUI wallet in windows](https://open.lbry.com/@NervaCurrency:9/nerva-gui-installation.2:3)_

_[How to install Nerva using command line interface on Windows](https://open.lbry.com/@NervaCurrency:9/nerva-cmd-installation-windows.2:0)_
	
## Mining Nerva on Windows

_[How to mine Nerva using command line interface](https://open.lbry.com/@NervaCurrency:9/mininig-cmd.2:c)_

_[How to use cmd line advance settings and make a quick run.bat file for mining](https://open.lbry.com/@NervaCurrency:9/cmd-advance.2:e)_

## Backing up Nerva on Windows	
_[How to backup Nerva Wallets in GUI Windows](https://open.lbry.com/@NervaCurrency:9/backup-wallet-GUI.2:7)_
	
_[How to backup Nerva database in Windows](https://open.lbry.com/@NervaCurrency:9/backup-nerva-database-in-windows.2:e)_

## Sending Nerva on Windows

_[How to transfer Nerva coins using command line interface](https://open.lbry.com/@NervaCurrency:9/transfer-cmd.2:e)_

_[How to transfer all Nerva to another wallet quickly using command line](https://open.lbry.com/@NervaCurrency:9/sweeping.2:2)_

## Nerva Wallets

_[How to create a read only wallet](https://open.lbry.com/@NervaCurrency:9/create-readonly-wallet.2:c)_

_[How to open a Nerva wallet in Command Line interface while GUI running in Windows](https://open.lbry.com/@NervaCurrency:9/open-wallet-cmd-with-GUI-.2:4)_

_[How to restore a Nerva wallet via cmd from a seed while GUI running in Windows](https://open.lbry.com/@NervaCurrency:9/faster-restore-seed-cmd-with-GUI.2:6)_

_[How to restore a Nerva wallet via cmd with private keys with GUI in Windows](https://open.lbry.com/@NervaCurrency:9/restore-from-keys-with-GUI.2:3)_

## Nerva Web Wallets

_[How to install Nerva web wallet easy way](https://open.lbry.com/@NervaCurrency:9/install-web-wallet.2:e)_

_[How to connect to a remote web wallet via SSH](https://open.lbry.com/@NervaCurrency:9/remote-web-wallet-ssh.2:a)_

## Nerva stuck

_[How to resolve stuck Nerva blockchain](https://open.lbry.com/@NervaCurrency:9/cmd-pop-some-blocks.2:5)_

## Nerva Testing Performance

_[Testing Nerva CPU mining performance of an 6th gen i5](https://open.lbry.com/@NervaCurrency:9/mining-testing-cpu-activity.2:a)_


# Linux

## Installing Nerva

_[How to install Nerva GUI in Ubuntu with quicksync option](https://open.lbry.com/@NervaCurrency:9/install-gui-ubuntu-quicksync:f)_

_[How to install Nerva web wallet in linux and usage](https://open.lbry.com/@NervaCurrency:9/install-web-wallet-in-linux:7)_

## Nerva Wallet

_[How to open Nerva wallet in command line in linux](https://open.lbry.com/@NervaCurrency:9/linux-open-wallet-cmd:2)_

# Mac

## Installing Nerva

_[How to install Nerva GUI on Mac with quick sync](https://open.lbry.com/@NervaCurrency:9/mac-nerva-install-gui-quicksync:9)_

## Installing Nerva web apps

_[How to configure Neva web miner app on Mac](https://open.lbry.com/@NervaCurrency:9/nerva-miner-web:c)_

# Application of Nerva

_[Simple Nerva shop demonstration using woo plugin for wordpress](https://open.lbry.com/@NervaCurrency:9/nerva-shop-demo:0)_

_[SDemostration of a simple web application game using Nerva coin](https://open.lbry.com/@NervaCurrency:9/simple-demostration-web-application.2:e)_
